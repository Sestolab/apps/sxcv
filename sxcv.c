#include <stdio.h>
#include <stdlib.h>
#include <xcb/xcb.h>
#include <xcb/xcb_icccm.h>


#if !defined(WINDOW_SIZE) || WINDOW_SIZE <= 0
#undef WINDOW_SIZE
#define WINDOW_SIZE 120
#endif


int main(int argc, char *argv[])
{
	if (argc < 2)
	{
		fprintf(stderr, "Usage: %s <hex-color>\n", argv[0]);
		return 1;
	}

	xcb_connection_t *c = xcb_connect(NULL, NULL);

	if (xcb_connection_has_error(c))
	{
		fprintf(stderr, "Could not connect to the X server.\n");
		return 1;
	}

	xcb_screen_t *s = xcb_setup_roots_iterator(xcb_get_setup(c)).data;
	xcb_drawable_t w = xcb_generate_id(c);
	uint32_t values[2] = { (uint32_t)(strtol(argv[1], NULL, 16)), XCB_EVENT_MASK_KEY_PRESS };

	xcb_create_window(c, XCB_COPY_FROM_PARENT, w, s->root, 0, 0, WINDOW_SIZE, WINDOW_SIZE, 0,
		XCB_WINDOW_CLASS_INPUT_OUTPUT, s->root_visual, XCB_CW_BACK_PIXEL|XCB_CW_EVENT_MASK, values);

	xcb_change_property(c, XCB_PROP_MODE_REPLACE, w, XCB_ATOM_WM_NAME, XCB_ATOM_STRING, 8, 4, "sxcv");
	xcb_change_property(c, XCB_PROP_MODE_REPLACE, w, XCB_ATOM_WM_CLASS, XCB_ATOM_STRING, 8, 4, "sxcv");

	xcb_size_hints_t hints;
	xcb_icccm_size_hints_set_min_size(&hints, WINDOW_SIZE, WINDOW_SIZE);
	xcb_icccm_size_hints_set_max_size(&hints, WINDOW_SIZE, WINDOW_SIZE);
	xcb_icccm_set_wm_size_hints(c, w, XCB_ATOM_WM_NORMAL_HINTS, &hints);

	xcb_map_window(c, w);
	xcb_flush(c);

	xcb_generic_event_t *e = NULL;
	do { e = xcb_wait_for_event(c); }
	while (e && (e->response_type & ~0x80) != XCB_KEY_PRESS);
	free(e);

	xcb_destroy_window(c, w);
	xcb_disconnect(c);
	return 0;
}


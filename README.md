# sxcv

Simple X Color Viewer.

## Dependencies

* libxcb
* xcb-util-wm

## Installation

* $ make WINDOW_SIZE=120
* # make install

## Examples

Display color, press any key to close the window:

```
$ sxcv 002b36
```

Pick a color using [sxcp](https://sestolab.pp.ua/apps/sxcp):

```
$ sxcp | xargs sxcv
```

```
$ sxcv $(sxcp)
```


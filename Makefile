WINDOW_SIZE ?= 120

CFLAGS += -Wall -Wextra -pedantic -lxcb -lxcb-icccm
CFLAGS += -DWINDOW_SIZE=$(WINDOW_SIZE)

PREFIX ?= /usr/local
CC ?= cc

all: sxcv

sxcv: sxcv.c
	$(CC) sxcv.c $(CFLAGS) -o sxcv

install: sxcv
	install -Dm755 sxcv -t $(DESTDIR)$(PREFIX)/bin

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/sxcv

clean:
	rm -f sxcv

.PHONY: all install uninstall clean

